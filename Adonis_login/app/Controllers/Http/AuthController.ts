import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import {schema,rules} from '@ioc:Adonis/Core/Validator'
import User from 'App/Models/User'
export default class AuthController {
    
    public async login({request,auth,response}:HttpContextContract){
        const validator = {
            schema: schema.create({
                email:schema.string({},[
                    rules.required(),
                    rules.email(),
                ]),
                password:schema.string({},[
                    rules.required(),
                    rules.minLength(6)
    
                ])
    
            }),
            messages:{
                'password.minLength':"The password must be at least 6 characters."
            }
        }
        const validated = await request.validate(validator)
    
            try {
                const data = await auth.use('api').attempt(validated.email, validated.password);
                return response.status(200).json({
                   message: "berhasil login",
                    data
    
              });
            } catch(err) {
                return response.status(401).json({
                    success: false,
                   message: "Invalid email or password"
                })
           
   
        }



    }
    

        
        
    
}
